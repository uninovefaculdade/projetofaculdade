<%@page import="entidade.Pessoa"%>
<%@page import="java.util.List"%>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>
        
        <div class="x_panel">
            <div class="x_title">
                <h2>Clientes <small>Lista de clientes Cadastrado</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            
            <div class="x_content">


                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="./cadastro_cliente.jsp" type="button" class="btn btn-primary">Adicionar Cliente</a></li>
                            <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 154px;">
                                            Nome</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 255px;">
                                            Endere�o</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 115px;">
                                            CEP</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 58px;">
                                            Telefone</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 112px;">
                                            E_mail</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 112px;">
                                            A��es</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 112px;">
                                            A��es</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <%
                                List<Pessoa> lista = (List<Pessoa>)request.getAttribute("buscaPessoa");

                                for(Pessoa pessoa:lista){
                            %>

                    

                                 <tr role="row" class="odd">
                                    <td class="sorting_1"><%=pessoa.getNome()%></td>
                                    <td><%=pessoa.getEndereco()%>, 
                                        <%=pessoa.getBairro()%>,N�
                                        <%=pessoa.getNumero()%>,
                                        <%=pessoa.getCidade()%>
                                    </td>
                                    <td><%=pessoa.getCEP()%></td>
                                    <td><%=pessoa.getTelefone()%></td>
                                    <td><%=pessoa.getE_mail()%></td>
                                    <td><a href="excluirdado.do?id=<%= pessoa.getId()%>" class="btn btn-danger">
                                            excluir</td>
                                    <td><a href="obterdados.do?id=<%= pessoa.getId()%>" class="btn btn-primary">
                                            Alterar</td>
                                   
                                 </tr>
                                    <%
                                        }
                                     %>
                                </tbody>
                    </table>
