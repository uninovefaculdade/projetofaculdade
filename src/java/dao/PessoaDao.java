/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import JDBC.Conexao;
import entidade.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Fernando-PC
 */
public class PessoaDao {
    
    public void cadastrar (Pessoa pessoa) {
        
        Connection con = Conexao.receberConexao();
        String sql = "INSERT INTO pessoa(id_tipo_pessoa,nome,CEP,endereco,numero,bairro,cidade,e_mail,telefone) VALUES (?,?,?,?,?,?,?,?,?)";
        
        try{
            PreparedStatement preparador = con.prepareStatement(sql);
            preparador.setInt(1, pessoa.getId_tipo_pessoa());
            preparador.setString(2, pessoa.getNome());
            preparador.setString(3, pessoa.getCEP());
            preparador.setString(4, pessoa.getEndereco());
            preparador.setString(5, pessoa.getNumero());
            preparador.setString(6, pessoa.getBairro());
            preparador.setString(7, pessoa.getCidade());
            preparador.setString(8, pessoa.getE_mail());
            preparador.setString(9, pessoa.getTelefone());
            
            
            
            preparador.execute();
            preparador.close();  
            
            System.out.println("Pessoa cadastrada com sucesso");
                         
        }catch (SQLException e){
            e.printStackTrace();
        }
                
        
    }
    
    public java.util.List<Pessoa> buscarTodos(){
        
        Connection con = Conexao.receberConexao();
        String sql = "SELECT *FROM pessoa";
        ArrayList<Pessoa> listaPessoa = new ArrayList<>();
        
        try{
            PreparedStatement preparador = con.prepareStatement(sql);
            ResultSet resultado = preparador.executeQuery();
            
            while(resultado.next()){
                Pessoa pessoa = new Pessoa();
                pessoa.setNome(resultado.getString("nome"));
                pessoa.setCEP(resultado.getString("CEP"));
                pessoa.setEndereco(resultado.getString("endereco"));
                pessoa.setNumero(resultado.getString("numero"));
                pessoa.setBairro(resultado.getString("bairro"));
                pessoa.setCidade(resultado.getString("cidade"));
                pessoa.setE_mail(resultado.getString("e_mail"));
                pessoa.setTelefone(resultado.getString("telefone"));
                pessoa.setId(resultado.getInt("id"));
                listaPessoa.add(pessoa);
            }
            
        }catch(SQLException ex){
            System.out.println("Erro é " + ex.getMessage());
            ex.getStackTrace();
            
        }
        return listaPessoa;
        
        
        
    }
    
        public void alterar (Pessoa pessoa){
        
        Connection con = Conexao.receberConexao();
        String sql = "UPDATE pessoa SET id_tipo_pessoa=?,nome=?,CEP=?,endereco=?,numero=?,bairro=?,cidade=?,e_mail=?,telefone=? WHERE id=?";
        
        try{
            PreparedStatement preparador = con.prepareStatement(sql);

            preparador.setInt(1, pessoa.getId_tipo_pessoa());
            preparador.setString(2, pessoa.getNome());
            preparador.setString(3, pessoa.getCEP());
            preparador.setString(4, pessoa.getEndereco());
            preparador.setString(5, pessoa.getNumero());
            preparador.setString(6, pessoa.getBairro());
            preparador.setString(7, pessoa.getCidade());
            preparador.setString(8, pessoa.getE_mail());
            preparador.setString(9, pessoa.getTelefone());
            preparador.setInt(10, pessoa.getId());
            
            
            
            preparador.execute();
            preparador.close();  
            
            System.out.println("Pessoa alterada com sucesso");
                         
        }catch (SQLException ex){
            ex.printStackTrace();
        }
                
        
    }
        
        public void excluir (Pessoa pessoa){
            
            Connection con = Conexao.receberConexao();
            
            String sql = "DELETE FROM pessoa WHERE id=?";
            
            try{
                PreparedStatement preparador = con.prepareStatement(sql);
                
                preparador.setInt(1, pessoa.getId());
                preparador.execute();
                preparador.close();
                
                System.out.println("pessoa excluida com sucesso");
                        
            }catch(SQLException ex){
                ex.getStackTrace();
            }
        }
        
        
        	public Pessoa buscarPorId(Integer id){
		
		Connection con = Conexao.receberConexao();
		
		String sql = "SELECT * from pessoa WHERE id = ?";
		
		Pessoa pessoa = null;
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setInt(1, id);
			
			ResultSet resultado = preparador.executeQuery();
			
			if(resultado.next()){
			pessoa = new Pessoa();
                                pessoa.setNome(resultado.getString("nome"));
                                pessoa.setCEP(resultado.getString("CEP"));
                                pessoa.setEndereco(resultado.getString("endereco"));
                                pessoa.setNumero(resultado.getString("numero"));
                                pessoa.setBairro(resultado.getString("bairro"));
                                pessoa.setCidade(resultado.getString("cidade"));
                                pessoa.setE_mail(resultado.getString("e_mail"));
                                pessoa.setTelefone(resultado.getString("telefone"));
			}
			
			con.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pessoa;
	}


    
}
