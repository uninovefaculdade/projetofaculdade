package Servlets;



import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PessoaDao;
import entidade.Pessoa;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/cadastro.do")
public class CadastroPessoa extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
                int id_pessoa = 1;
		String nome = request.getParameter("name");
                String cep = request.getParameter("cep");
                String endereco = request.getParameter("endereco");
                String numero = request.getParameter("numero");
		String bairro = request.getParameter("bairro");
                String cidade = request.getParameter("cidade");
		String email = request.getParameter("email");
                String telefone = request.getParameter("telefone");

                
		
		Pessoa pessoa = new Pessoa();
                pessoa.setId_tipo_pessoa(id_pessoa);
		pessoa.setNome(nome);
                pessoa.setCEP(cep);
                pessoa.setEndereco(endereco);
                pessoa.setNumero(numero);
                pessoa.setBairro(bairro);
                pessoa.setCidade(cidade);
                pessoa.setE_mail(email);
		pessoa.setTelefone(telefone);
		
		
                
		PessoaDao pessoaDao = new PessoaDao();
                pessoaDao.cadastrar(pessoa);
                
  
		
		response.sendRedirect("./buscartodos.do");
		
	}

}
