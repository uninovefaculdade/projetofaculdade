/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import static com.sun.corba.se.impl.util.Utility.printStackTrace;
import dao.PessoaDao;
import entidade.Pessoa;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando-PC
 */
@WebServlet("/buscartodos.do")
public class buscarTodos extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		PessoaDao pessoaDao = new PessoaDao();
		List<Pessoa> buscaPessoa = pessoaDao.buscarTodos();
                      
                      	
		request.setAttribute("buscaPessoa", buscaPessoa);
		request.getRequestDispatcher("cliente.jsp").forward(request, response);

	}


}








