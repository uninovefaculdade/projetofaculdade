package Servlets;


import dao.PessoaDao;
import entidade.Pessoa;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/obterdados.do")
public class AlterarDados extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		PessoaDao pessoaDao = new PessoaDao();
		Pessoa pessoa = pessoaDao.buscarPorId(id);
		pessoa.setId(id);
		
		request.setAttribute("pessoa", pessoa);
		request.getRequestDispatcher("alterar_cliente.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
                int id_pessoa = 1;
                int id = Integer.parseInt(request.getParameter("id"));
		String nome = request.getParameter("name");
                String cep = request.getParameter("cep");
                String endereco = request.getParameter("endereco");
                String numero = request.getParameter("numero");
		String bairro = request.getParameter("bairro");
                String cidade = request.getParameter("cidade");
		String email = request.getParameter("email");
                String telefone = request.getParameter("telefone");
		
		Pessoa pessoa = new Pessoa();
                pessoa.setId_tipo_pessoa(id_pessoa);
                pessoa.setId(id);
		pessoa.setNome(nome);
                pessoa.setCEP(cep);
                pessoa.setEndereco(endereco);
                pessoa.setNumero(numero);
                pessoa.setBairro(bairro);
                pessoa.setCidade(cidade);
                pessoa.setE_mail(email);
		pessoa.setTelefone(telefone);
		
		PessoaDao pessoaDao = new PessoaDao();
                pessoaDao.alterar(pessoa);
		
		response.sendRedirect("./buscartodos.do");
		
	}	
}