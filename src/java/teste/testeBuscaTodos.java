/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import dao.PessoaDao;
import entidade.Pessoa;
import java.util.List;
import org.apache.jasper.tagplugins.jstl.ForEach;

/**
 *
 * @author Fernando-PC
 */
public class testeBuscaTodos {
    public static void main(String[] args){
        
        PessoaDao pessoaDao = new PessoaDao();
        List<Pessoa> buscaPessoa = pessoaDao.buscarTodos();
        for(Pessoa pessoa:buscaPessoa){
        System.out.println("Nome: " + pessoa.getNome());
        System.out.println("Telefone: " + pessoa.getTelefone());
        System.out.println("Endereço: " + pessoa.getEndereco());
        System.out.println("Numero: " + pessoa.getNumero());
        
        }
        
    }
    
}
