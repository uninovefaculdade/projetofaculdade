-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Maio-2018 às 15:35
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzaria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL,
  `id_tipo_pessoa` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `telefone` varchar(11) DEFAULT NULL,
  `e_mail` varchar(50) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `numero` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id`, `id_tipo_pessoa`, `nome`, `telefone`, `e_mail`, `CEP`, `bairro`, `endereco`, `cidade`, `numero`) VALUES
(8, 1, 'TESThjkhjkE 5', '982646351', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 1, 'Luiz Fernando Santos da SIlva', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 1, 'Luiz Fernando Santos da SIlva', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 'Luiz Fernando Santos da SIlva', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 'name', 'telefone', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 1, 'Luiz Fernando Santos da Silva', '11969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 1, 'Luiz Fernando Santos da SIlva', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 'Luiz Fernando Santos da SIlva', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 'TESTE', '969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 'Luiz Fernando Santos da Silva', '11969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 'Marce', 'dsa', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 1, 'Luiz Fernando Santos da Silva', '11969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 'Luiz Fernando Santos da Silva', '11969499522', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 'name', 'telefone', 'email', 'cep', 'bairro', 'endereco', 'cidade', 'numero'),
(26, 1, 'Luiz Fernando Santos da Silva', '11969499522', 'nandofernando100@hotmail.com', '05773080', 'Parque Regina', 'Praça João Pais Malio', 'São Paulo', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pessoas` (`id_tipo_pessoa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD CONSTRAINT `fk_pessoas` FOREIGN KEY (`id_tipo_pessoa`) REFERENCES `tipo_pessoa` (`id_pessoa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
