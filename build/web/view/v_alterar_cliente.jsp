<%@page import="entidade.Pessoa"%>
<%

	Pessoa pessoa = (Pessoa)request.getAttribute("pessoa");

%>

<div class="right_col" role="main">
    <div class="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate action="obterdados.do" method="post">
                      <span class="section">Alterar Cliente</span>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="id" 
                                 value="<%=pessoa.getId()%>"
                                 placeholder="Digite o Nome"  required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" 
                                 value="<%=pessoa.getNome()%>" placeholder="Digite o Nome" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">CEP <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-3 col-xs-12">
                          <input id="CEP" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" data-validate-words="3" name="cep" 
                                 value="<%=pessoa.getCEP()%>" placeholder="Digite o CEP" required="required" type="text">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Endere�o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="endereco" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="endereco" 
                                 value="<%=pessoa.getEndereco()%>" placeholder="Digite o Endere�o" required="required" type="text">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Numero <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="numero" name="numero" 
                                 value="<%=pessoa.getNumero()%>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Bairro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="bairro" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="bairro" 
                                 value="<%=pessoa.getBairro()%>" placeholder="Digite o Bairro" required="required" type="text">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cidade <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="cidade" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="cidade"
                                 value="<%=pessoa.getCidade()%>" placeholder="Digite a cidade" required="required" type="text">
                        </div>
                      </div>                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" 
                                 value="<%=pessoa.getE_mail()%>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telefone <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="tel" id="telephone" name="telefone" 
                                 value="<%=pessoa.getTelefone()%>" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 

                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Alterar</button>
                        </div>
                      </div>
                    </form>
                  </div>
    
    